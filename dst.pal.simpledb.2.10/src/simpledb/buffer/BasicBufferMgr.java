package simpledb.buffer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import simpledb.file.*;

/**
 * CS4432-Project1: Policy types
 */
enum PolicyType {
	SCAN,
	LRU,
	CLOCK
}

/**
 * Manages the pinning and unpinning of buffers to blocks.
 * @author Edward Sciore
 *
 */
class BasicBufferMgr {
	/**
	 * CS4432-Project1: Array of buffer frames
	 **/
   private Buffer[] bufferpool;
   /** 
    *  CS4432-Project1: Number of unpinned frames
    **/
   private int numAvailable;
   /** 
    *  CS4432-Project1: Maintain bitmap for quickly determining empty frame 
    *  0 = empty, 1 = full 
    **/
   private int[] frameBitmap;
   /** 
    *  CS4432-Project1: Maintain hash table to find given disk block
    *  <Block, BufferId>
    **/
   private HashMap<Block, Integer> blockTable;
   /** 
    *  CS4432-Project1: Used to choose the policy
    **/
   private PolicyType policy;
   /** 
    *  CS4432-Project1: Maintain Queue of least recently used buffer (bufferIndex)
    *  Buffers are added to the queue when their buffer is fully unpinned
    **/
   private Queue<Integer> LRUQueue;
   /**
    * CS4432-Project1: Maintain Clock index for Clock Policy
    * A function will maintain the circular indexing of the clock
    */
   private int clockIndex;
   
   /**
    * Creates a buffer manager having the specified number 
    * of buffer slots.
    * This constructor depends on both the {@link FileMgr} and
    * {@link simpledb.log.LogMgr LogMgr} objects 
    * that it gets from the class
    * {@link simpledb.server.SimpleDB}.
    * Those objects are created during system initialization.
    * Thus this constructor cannot be called until 
    * {@link simpledb.server.SimpleDB#initFileAndLogMgr(String)} or
    * is called first.
    * @param numbuffs the number of buffer slots to allocate
    */
	BasicBufferMgr(int numbuffs) {
		bufferpool = new Buffer[numbuffs];
		numAvailable = numbuffs;
		frameBitmap = new int[numbuffs];
		for (int i = 0; i < numbuffs; i++) {
			bufferpool[i] = new Buffer(i);
			/*
			 * CS4432-Project1: Initialize frames to empty
			 **/
			frameBitmap[i] = 0;
		}
		/*
		 * CS4432-Project1: Initialize block table
		 **/
		blockTable = new HashMap<Block, Integer>();
		/*
		 * CS4432-Project1: Choose PolicyType
		 **/
		policy = PolicyType.SCAN;
		//policy = PolicyType.LRU;
		//policy = PolicyType.CLOCK;
		/*
		 * CS4432-Project1: Create queue of LRU buffers (Integer = bufferIndex)
		 **/
		LRUQueue = new LinkedList<Integer>();
		/*
		 * CS4432-Project1: Initialize clockIndex to the beginning of the bufferpool
		 **/
		clockIndex = 0;
	}
   
   /**
    * Flushes the dirty buffers modified by the specified transaction.
    * @param txnum the transaction's id number
    * 
    * CS4332-Project1: When a transaction finishes, it flushes
    * any frames that it modified
    * (Task 2.4)
    */
   synchronized void flushAll(int txnum) {
      for (Buffer buff : bufferpool)
         if (buff.isModifiedBy(txnum))
        	 buff.flush();
   }
   
   /**
    * Pins a buffer to the specified block. 
    * If there is already a buffer assigned to that block
    * then that buffer is used;  
    * otherwise, an unpinned buffer from the pool is chosen.
    * Returns a null value if there are no available buffers.
    * @param blk a reference to a disk block
    * @return the pinned buffer
    * 
    * CS4332-Project1: Pins the buffer in given block
    * (Task 2.4)
    */
   synchronized Buffer pin(Block blk) {
      Buffer buff = findExistingBuffer(blk);
      /*
       * CS4432-Project1: Keep track of oldBlock
       * to be able to remove from hashmap
       */
      Block oldBlock = null;
      if (buff == null) {
         buff = chooseUnpinnedBuffer();
         if (buff == null)
            return null;
         oldBlock = buff.block();
         buff.assignToBlock(blk);
      }
      if (!buff.isPinned()) {
         numAvailable--;
         if (policy == PolicyType.LRU)
        	 LRUQueue.remove(buff.getId());
      }
      buff.pin();
      /*
       *  CS4432-Project1: Add block to blockTable
       */
      if (blockTable.containsValue(buff.getId())) {
    	// CS4432-Project1: remove old block entry
     	 blockTable.remove(oldBlock);
      }
      blockTable.put(blk, buff.getId());
      //printMap(blockTable);
      /*
       *  CS4432-Project1: Print Bufferpool state
       */
      System.out.println("Pinning Buffer: " + buff.getId());
      //System.out.println(toString());
      return buff;
   }
   
   /**
    * Allocates a new block in the specified file, and
    * pins a buffer to it. 
    * Returns null (without allocating the block) if 
    * there are no available buffers.
    * @param filename the name of the file
    * @param fmtr a pageformatter object, used to format the new block
    * @return the pinned buffer
    */
	synchronized Buffer pinNew(String filename, PageFormatter fmtr) {
		Buffer buff = chooseUnpinnedBuffer();
		Block oldBlock = buff.block();
		if (buff == null)
			return null;
		buff.assignToNew(filename, fmtr);
		numAvailable--;
		buff.pin();
		/*
         *  CS4432-Project1: Add block to blockTable
         *  Crash happening here...
         **/
		if (blockTable.containsValue(buff.getId())) {
	    	 // remove old block entry
	     	 blockTable.remove(oldBlock);
	      }
	      blockTable.put(buff.block(), buff.getId());
	      //printMap(blockTable);
		/*
		 * CS4432-Project1: Print Bufferpool state
		 */
		System.out.println("Pinning new Buffer: " + buff.getId());
		//System.out.println(toString());
		return buff;
	}
   
   /**
    * Unpins the specified buffer.
    * @param buff the buffer to be unpinned
    */
   synchronized void unpin(Buffer buff) {
      buff.unpin();
      if (!buff.isPinned()) {
         numAvailable++;
         /*
          *  CS4432-Project1: Keep track of least recently used block
          *  by adding block number to the LRUQueue as they are unpinned
          **/
         if (policy == PolicyType.LRU) {
        	 if (LRUQueue.contains(buff.getId()))
        		 LRUQueue.remove(buff.getId());
        	 LRUQueue.add(buff.getId());
         }
      }
      /*
       *  CS4432-Project1: Print Bufferpool state
       */
      System.out.println("Unpinning Buffer: " + buff.getId());
      //System.out.println(toString());
   }
   
   /**
    * Returns the number of available (i.e. unpinned) buffers.
    * @return the number of available buffers
    */
	int available() {
		return numAvailable;
	}
   
	private Buffer findExistingBuffer(Block blk) {
		/*
		 * CS4432-Project1: Attempt to find buffer using blockTable
		 * (Task 2.2)
		 **/
		Buffer blockBuffer;
		System.out.println("Looking in blockTable for: " + blk.toString());
		printMap(blockTable);
		if (blockTable.containsKey(blk)) {
			if ((blockBuffer = bufferpool[blockTable.get(blk)]) != null) {
				System.out.println("blockTable found buffer: " + blockBuffer.getId() + ", for block: [" + blk.toString() + "]");
				return blockBuffer;
			}
		}

		// Buffer Scanning
		for (Buffer buff : bufferpool) {
			Block b = buff.block();
			if (b != null && b.equals(blk)) {
				System.out.println("Buffer block scanning found buffer: " + buff.getId());
				return buff;
			}
		}
		return null;
	}
   
	private Buffer chooseUnpinnedBuffer() {
		// CS4432-Project1: Print buffer before choosing
		System.out.println("");
		System.out.println("Policy choosing Unpinned Buffer from:");
		System.out.println(toString());
		/*
		 * CS4432-Project1: Attempt to find empty buffer first if one exists
		 * (Task 2.1)
		 **/
		Buffer emptyBuff;
		if ((emptyBuff = findEmptyFrame()) != null) {
			System.out.println("Chose empty buffer: " + emptyBuff.getId());
			return emptyBuff;
		}
		
		/*
		 * CS4432-Project1: Use Policy to choose next buffer
		 * (Task 2.3)
		 **/
		switch(policy) {
		case CLOCK:
			return chooseClockBuffer();
		case LRU:
			return chooseLRUBuffer();
		}
		
		/*
		 * CS4432-Project1: If an empty buffer doesn't exist,
		 * and neither of the policies returned a buffer,
		 * scan for an unpinned buffer
		 **/
		for (Buffer buff : bufferpool)
			if (!buff.isPinned()) {
				System.out.println("Scanning chose unpinned buffer: " + buff.getId());
				return buff;
			}
		return null;
	}
   
   /** 
    *  CS4432-Project1: Find an empty frame in constant time
    *  using the frameBitmap
    *  (Task 2.1)
    **/
	private Buffer findEmptyFrame() {
		for (int i = 0; i < frameBitmap.length; i++) {
			if (frameBitmap[i] == 0) {
				// CS4432-Project1: Frame is going to be filled after every
				// call of this function
				frameBitmap[i] = 1;
				return bufferpool[i];
			}
		}
		return null;
	}
	
	/**
	 * CS4432-Project1: Return the least recently used, unpinned, buffer
	 * (Task 2.3 LRU)
	 **/
	private Buffer chooseLRUBuffer() {
		// CS4432-Project1: Print out queue
		System.out.print("LRUQueue: [");
		for (int i = 0; i < LRUQueue.size(); i++) {
			System.out.print(LRUQueue.toArray()[i]);
			if (i < LRUQueue.size()-1)
				System.out.print(", ");
		}
		System.out.println("]");
		// CS4432-Project1: Choose head of queue
		Buffer LRUBuffer = bufferpool[LRUQueue.poll()];
		System.out.println("LRU Policy chose unpinned buffer: " + LRUBuffer.getId());
		return LRUBuffer;
	}
	
	/**
	 * CS4432-Project1: Return the least recently used, unpinned, buffer
	 * (Task 2.3 Clock)
	 **/
	private synchronized Buffer chooseClockBuffer() {
		while (true) {
			Buffer currentBuffer = bufferpool[clockIndex];
			// CS4432-Project1: Only check unpinned frames
			if (!currentBuffer.isPinned()) {
				// CS4432-Project1: refbit is 1
				if (currentBuffer.getRefbit() != 0) {
					// CS4432-Project1: set refbit to 0
					currentBuffer.setRefbit(0);
					// continue around the clock
					if (clockIndex == (bufferpool.length - 1))
						clockIndex = 0;
					else clockIndex++;
				} else {
					// CS4432-Project1: This frame fits all conditions
					System.out.println("Clock Policy chose unpinned buffer: " + currentBuffer.getId());
					// CS4432-Project1: reset refbit
					currentBuffer.setRefbit(1);
					return currentBuffer;
				}
			} else {
				// CS4432-Project1: frame is pinned, continue to next clock tick
				if (clockIndex == (bufferpool.length - 1))
					clockIndex = 0;
				else clockIndex++;
			}
		}
	}
	
	/**
	 * CS4432-Project1: Returns a string representation of the
	 * bufferpool
	 * (Task 2.5)
	 **/
	public String toString() {
		String string = "";
		for (Buffer buffer : bufferpool) {
			string = string.concat(buffer.toString() + "\n");
		}
		return string;
	}
	
	/**
	 * CS4432-Project1: Prints out the blockTable for
	 * testing purposes
	 **/
	public static void printMap(Map mp) {
	    Iterator it = mp.entrySet().iterator();
	    System.out.println("blockTable:");
	    System.out.println("=====================================");
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.println(pair.getKey().toString() + " = " + pair.getValue());
	    }
	    System.out.println("=====================================");
	}
}


