Dalton Tapply (dstapply)
Peter Leondires (paleondires)

All of the testing and turnin files are in the /testing/ directory and the initial project .zip

To run our modified version of simpleDB:
	1.) Download the project
	2.) Import the project into eclipse
	3.) Check that the build path includes the weblaf-complete-1.28.jar. If it doesn't please modify the build path and add this external jar. 
	4.) Choose the policy type in the BasicBufferMgr.java file on line 90-92
	5.) (Like the original simpleDB configuration) Run startup.java, as a java application with a string argument set, to start the simpleDB server.
	6.) Next run the testClient in testClient/jdbcClient/Client.java. This can be simply ran as a java application.
	7.) To test functionality, watch the console of the server, after clicking the createTables and populate tables buttons.
	8.) Selecting can also be tested by running the example select queries in the query panel, by clicking the Run Query button.
	9.) You can change the policy type and repeat the process for each policy.
	10.) Sample output files are in the home directory and the /testing/ directory