
CREATE TABLE Persons
(
PersonID int,
LastName varchar(20),
FirstName varchar(20),
Address varchar(20),
City varchar(20)
);

CREATE TABLE Stuff
(
StuffID int,
StuffName varchar(20),
StuffValue int
);

INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (0, 'Tapply', 'Dalton', '432 Greenville Rd.', 'New Ipswich');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (1, 'Tapply', 'Cory', '432 Greenville Rd.', 'New Ipswich');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (2, 'Leondires', 'Peter', 'Somewhere', 'Atkin');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (3, 'Smith', 'Bill', 'Place Rd.', 'Place1');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (4, 'Smith', 'Ryan', 'Place3 Rd.', 'Place3');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (5, 'Peterson', 'Brian', 'Place2 Rd.', 'Place2');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (6, 'Peterson', 'Cheryl', 'Boston Rd.', 'Boston');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (7, 'Crowley', 'Cari', '432 Boston Rd.', 'Boston');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (8, 'Lastname', 'Firstname', 'New York City Rd.', 'New York City');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (9, 'Test1', 'Test2', 'New Ipswich Rd.', 'Greenville');
INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (10, 'Final', 'Names', '10 Faraday St.', 'Worcester');

INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (0, 'Some stuff', 5);
INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (1, 'Some other stuff', 34);
INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (2, 'New stuff', 33);
INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (3, 'Old stuff', 63);
INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (4, 'Meh', 5);
INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (5, 'Stufferino', 11);

// Sample select statements

SELECT PersonID, LastName, FirstName, Address, City FROM Persons;
SELECT PersonID, LastName, FirstName, Address, City FROM Persons WHERE FirstName = 'Ryan';
SELECT StuffID, StuffName, StuffValue FROM Stuff;
SELECT StuffName, StuffValue FROM Stuff WHERE StuffName = 'Stufferino';

