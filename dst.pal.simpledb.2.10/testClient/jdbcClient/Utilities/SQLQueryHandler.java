package jdbcClient.Utilities;

import java.sql.*;
import simpledb.remote.SimpleDriver;

public class SQLQueryHandler {
	
	public static String executeQuery(String query) {
		String output = "";
		Connection conn = null;
		try {
			Driver d = new SimpleDriver();
			conn = d.connect("jdbc:simpledb://localhost", null);
			Statement stmt = conn.createStatement();
			if (query.toLowerCase().startsWith("select")) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) {
					output = output.concat("[ ");
					if (query.toLowerCase().contains("from persons")) {
						if (query.contains("PersonID")) {
							int id = rs.getInt("PersonID");
							output = output.concat("PersonID: " + id + ", ");
						}
						if (query.contains("FirstName")) {
							String firstName = rs.getString("FirstName");
							output = output.concat("FirstName: " + firstName + ", ");
						}
						if (query.contains("LastName")) {
							String lastName = rs.getString("LastName");
							output = output.concat("LastName: " + lastName + ", ");
						}
						if (query.contains("Address")) {
							String address = rs.getString("Address");
							output = output.concat("Address: " + address + ", ");
						}
						if (query.contains("City")) {
							String city = rs.getString("City");
							output = output.concat("City: " + city + ", ");
						}
					} else if (query.toLowerCase().contains("from stuff")) {
						if (query.contains("StuffID")) {
							int id = rs.getInt("StuffID");
							output = output.concat("StuffID: " + id + ", ");
						}
						if (query.contains("StuffName")) {
							String stuffName = rs.getString("StuffName");
							output = output.concat("StuffName: " + stuffName + ", ");
						}
						if (query.contains("StuffValue")) {
							int val = rs.getInt("StuffValue");
							output = output.concat("StuffValue: " + val + ", ");
						}
					}
					if (output.length() > 3)
						output = output.substring(0, output.length()-2);
					output = output.concat(" ]\n");
				}
				rs.close();
			} else {
				System.out.println("Executing Query: " + query);
				stmt.executeUpdate(query);
				System.out.println("Query Executed Successfully");
				output = "Query Executed Successfully";
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			System.out.println("Query Execution Failed");
			output = "Query Execution Failed";
		}
		finally {
			try {
				if (conn != null)
					conn.close();
			}
			catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return output;
	}
}
