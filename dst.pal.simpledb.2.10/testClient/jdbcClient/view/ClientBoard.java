package jdbcClient.view;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SpringLayout;

import jdbcClient.Utilities.SQLQueryHandler;
import jdbcClient.Utilities.SpringUtilities;

public class ClientBoard extends JPanel implements ActionListener {
	private JFrame frame;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem queryMenuItem, tableMenuItem, searchMenuItem, testMenuItem, homeMenuItem;
	private JButton runQueryButton, createTableButton, searchButton, populatePersonsButton,
					populateStuffButton;
	private QueryPanel queryPanel;
	private JPanel currentlyVisiblePanel;
	
	public ClientBoard(JFrame frame) {
		this.frame = frame;
		//initilize panels
		currentlyVisiblePanel = this;
		queryPanel = new QueryPanel(frame);
		
		// initialize layout
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		// Create menu bar
		menuBar = new JMenuBar();
		
		// Create menu
		menu = new JMenu("Menu");
		menuBar.add(menu);
		
		// Create menu items
		queryMenuItem = new JMenuItem("Run Query");
		queryMenuItem.addActionListener(this);
		queryMenuItem.setActionCommand("runQuery");
		menu.add(queryMenuItem);
		
		homeMenuItem = new JMenuItem("Home");
		homeMenuItem.addActionListener(this);
		homeMenuItem.setActionCommand("home");
		
		// Set menubar
		frame.setJMenuBar(menuBar);
		
		// Add buttons
		runQueryButton = new JButton("Run Query");
	    runQueryButton.setMnemonic(KeyEvent.VK_R);
	    runQueryButton.setActionCommand("runQuery");
	    runQueryButton.addActionListener(this);
	    add(runQueryButton);
	    
	    createTableButton = new JButton("Create Tables");
	    createTableButton.addActionListener(this);
	    createTableButton.setMnemonic(KeyEvent.VK_C);
	    createTableButton.setActionCommand("createTables");
	    add(createTableButton);
	    
	    populatePersonsButton = new JButton("Populate Persons");
	    populatePersonsButton.addActionListener(this);
	    populatePersonsButton.setMnemonic(KeyEvent.VK_T);
	    populatePersonsButton.setActionCommand("populatePersons");
	    add(populatePersonsButton);
	    
	    populateStuffButton = new JButton("Populate Stuff");
	    populateStuffButton.addActionListener(this);
	    populateStuffButton.setMnemonic(KeyEvent.VK_T);
	    populateStuffButton.setActionCommand("populateStuff");
	    add(populateStuffButton);
	    
	    searchButton = new JButton("Search");
	    searchButton.addActionListener(this);
	    searchButton.setMnemonic(KeyEvent.VK_S);
	    searchButton.setActionCommand("search");
	    add(searchButton);
	    //Not implemented
	    searchButton.setVisible(false);
	    
	    // Easy Grid layout for buttons
	    SpringUtilities.makeCompactGrid(this, 5, 1, 100, 100, 6, 6);
	}
	
	public void setCurrentlyVisiblePanel(JPanel panel) {
		// Add menu item back
		if (currentlyVisiblePanel.equals(queryPanel)) {
			menu.add(queryMenuItem, 0);
		} else if (currentlyVisiblePanel.equals(this)) {
			menu.add(homeMenuItem, 0);
		}
		// Remove new visible panel menu item
		if (panel.equals(queryPanel)) {
			menu.remove(queryMenuItem);
		} else if (panel.equals(this)) {
			menu.remove(homeMenuItem);
		}
		// set visibility
		currentlyVisiblePanel.setVisible(false);
		frame.remove(currentlyVisiblePanel);
		currentlyVisiblePanel = panel;
		frame.add(panel);
		currentlyVisiblePanel.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// home button pressed
		if (e.getActionCommand().equals("home")) {
			setCurrentlyVisiblePanel(this);
		}
		// query button pressed
		if (e.getActionCommand().equals("runQuery")) {
			setCurrentlyVisiblePanel(queryPanel);
		}
		// create tables button pressed
		if (e.getActionCommand().equals("createTables")) {
			SQLQueryHandler.executeQuery(
					"CREATE TABLE Persons(PersonID int, LastName varchar(20), FirstName varchar(20), Address varchar(20), City varchar(20));");
			SQLQueryHandler.executeQuery(
					"CREATE TABLE Stuff(StuffID int, StuffName varchar(20), StuffValue int));");
		}
		// populate persons pressed
		if (e.getActionCommand().equals("populatePersons")) {
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (0, 'Tapply', 'Dalton', '432 Greenville Rd.', 'New Ipswich');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (1, 'Tapply', 'Cory', '432 Greenville Rd.', 'New Ipswich');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (2, 'Leondires', 'Peter', 'Somewhere', 'Atkin');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (3, 'Smith', 'Bill', 'Place Rd.', 'Place1');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (4, 'Smith', 'Ryan', 'Place3 Rd.', 'Place3');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (5, 'Peterson', 'Brian', 'Place2 Rd.', 'Place2');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (6, 'Peterson', 'Cheryl', 'Boston Rd.', 'Boston');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (7, 'Crowley', 'Cari', '432 Boston Rd.', 'Boston');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (8, 'Lastname', 'Firstname', 'New York City Rd.', 'New York City');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (9, 'Test1', 'Test2', 'New Ipswich Rd.', 'Greenville');");
			SQLQueryHandler.executeQuery("INSERT INTO Persons(PersonID, LastName, FirstName, Address, City) VALUES (10, 'Final', 'Names', '10 Faraday St.', 'Worcester');");
		}
		// populate stuff pressed
		if (e.getActionCommand().equals("populateStuff")) {
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (0, 'Some stuff', 5);");
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (1, 'Some other stuff', 34);");
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (2, 'New stuff', 33);");
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (3, 'Old stuff', 63);");
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (4, 'Meh', 5);");
			SQLQueryHandler.executeQuery("INSERT INTO Stuff(StuffID, StuffName, StuffValue) VALUES (5, 'Stufferino', 11);");
		}
	}
}