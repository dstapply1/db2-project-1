package jdbcClient.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Spring;
import javax.swing.SpringLayout;

import jdbcClient.Utilities.SQLQueryHandler;
import jdbcClient.Utilities.SpringUtilities;

public class QueryPanel extends JPanel implements ActionListener {
	private JLabel queryLabel, outputLabel;
	private JTextArea queryText, outputText;
	private JButton execute;
	private JFrame frame;
	
	public QueryPanel(JFrame frame) {
		this.frame = frame;
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		JPanel queryPanel = new JPanel();
		SpringLayout queryLayout = new SpringLayout();
		queryPanel.setLayout(queryLayout);
		SpringLayout.Constraints queryPanelCons = layout.getConstraints(queryPanel);
		queryPanelCons.setX(Spring.constant(10));
		queryPanelCons.setY(Spring.constant(10));
		JPanel outputPanel = new JPanel();
		SpringLayout outputLayout = new SpringLayout();
		outputPanel.setLayout(outputLayout);
		SpringLayout.Constraints outputPanelCons = layout.getConstraints(outputPanel);
		outputPanelCons.setX(Spring.constant(10));
		outputPanelCons.setY(Spring.constant(180));
		
		queryLabel = new JLabel("Query: ");
		queryPanel.add(queryLabel);
		
		queryText = new JTextArea(5, 20);
		JScrollPane queryScrollPane = new JScrollPane(queryText);
		queryText.setLineWrap(true);
		queryText.setWrapStyleWord(true);
		queryPanel.add(queryScrollPane);
		
		SpringUtilities.makeCompactGrid(queryPanel, 2, 1, 5, 5, 6, 6);
		
		execute = new JButton("Execute");
		execute.addActionListener(this);
		execute.setMnemonic(KeyEvent.VK_E);
		execute.setActionCommand("execute");
		SpringLayout.Constraints executeCons = layout.getConstraints(execute);
		executeCons.setX(Spring.constant(10));
		executeCons.setY(Spring.constant(140));
		add(execute);
		
		outputLabel = new JLabel("Output: ");
		outputPanel.add(outputLabel);
		
		outputText = new JTextArea(8, 26);
		JScrollPane outputScrollPane = new JScrollPane(outputText);
		queryText.setLineWrap(true);
		queryText.setWrapStyleWord(true);
		outputText.setEditable(false);
		outputPanel.add(outputScrollPane);
		
		SpringUtilities.makeCompactGrid(outputPanel, 2, 1, 5, 5, 6, 6);
		
		add(queryPanel);
		add(outputPanel);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("execute")) {
			String output = SQLQueryHandler.executeQuery(queryText.getText());
			outputText.setText(output);
		}
	}

}
