package jdbcClient;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIManager;

import jdbcClient.view.ClientBoard;

public class Client {
	private static JFrame frame;
	private static ClientBoard clientBoard;
	
	private static void initClient() {
		frame = new JFrame();
		frame.setTitle("JDBC Client");
		frame.setSize(300, 400);
		frame.setLocation(600, 250);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		clientBoard = new ClientBoard(frame);
		
		frame.add(clientBoard);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.alee.laf.WebLookAndFeel");
					initClient();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

}
