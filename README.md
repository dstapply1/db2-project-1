# README #

This was the first project for Advanced Database Systems at WPI. 

It involved adding database optimizations to simpleDB, which is a stripped down database for academic purposes.

**Implementations:**

1.) Maintained bitmap of empty frames to speed up empty frame search.

2.) Maintained a hashmap of block -> buffer, to enable near instant search for a block given a buffer

3.) Least Recently Used Policy for picking unpinned buffers for replacement.

4.) Clock Policy for picking unpinned buffers for replacement.